from django.shortcuts import render
from .forms import Settings_Form

response = {}
def index(request):
    response['settings_form'] = Settings_Form
    return render(request, 'settings_app.html', response)


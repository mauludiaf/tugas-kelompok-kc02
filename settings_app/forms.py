import django.forms

class Settings_Form(django.forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'class': 'form-control'
    }

    username = django.forms.CharField(label='Username', required=True, widget=django.forms.TextInput(attrs=attrs))
    old_password = django.forms.PasswordInput()
    new_password = django.forms.PasswordInput()
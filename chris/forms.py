from django import forms
from django.forms import widgets

class ForgotForm(forms.Form):
	email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={"class": "right"}))

class SigninForm(forms.Form):
	email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={"class": "right"}))
	password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}), max_length=15)

class SignupForm(forms.Form):
	username = forms.CharField(label="Username", widget=forms.TextInput(attrs={"class": "right"}), max_length=15)
	email = forms.EmailField(label="Email", widget=forms.EmailInput(attrs={"class": "right"}))
	password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}), max_length=15)
	confirm = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={"class": "right"}), max_length=15)
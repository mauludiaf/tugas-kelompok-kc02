from django.urls import path
from . import views

urlpatterns = [
    path('signin/', views.signin, name='signin'),
    path('signup/', views.signup, name='signup'),
    path('forgot/', views.forgot, name='forgot'),
    path(r'^(?P<username>[\w.@+-]+)/$', views.dashboard, name='dashboard'),
]
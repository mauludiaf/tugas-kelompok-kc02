from django.shortcuts import render
from django.http import HttpResponse
from .models import Users
from .forms import SigninForm, SignupForm, ForgotForm
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from django.utils.crypto import get_random_string

# Create your views here.
def signin(request):
	if request.method == 'POST':
		form = SigninForm(request.POST)
		if form.is_valid():
			e = form.cleaned_data['email']
			p = form.cleaned_data['password']
			try:
				pwd = getattr(Users.objects.get(email=e), 'password')
				if p != pwd:
					return HttpResponse("Wrong password!" + pwd)
			except:
				return render(request, 'signin.html', {'form': form, 'response': "Account not found!"})
			return render(request, 'dashboard.html', {'username': getattr(Users.objects.get(email=e), 'username')})
	else:
		form = SigninForm()
	return render(request, 'signin.html', {'form': form, 'response': ""})

def signup(request):
	if request.method == 'POST':
		form = SignupForm(request.POST)
		if form.is_valid():
			u = form.cleaned_data['username']
			e = form.cleaned_data['email']
			p = form.cleaned_data['password']
			cp = form.cleaned_data['confirm']
			if p != cp:
				return render(request, 'signup.html', {'form': form, 'response': "Password not match!"})
			if Users.objects.filter(email = e).count() > 0:
				return render(request, 'signup.html', {'form': form, 'response': "Email has been registered before!"})
			user = Users(username=u, email=e, password=p, location="null")			
			user.save()

			return render(request, 'dashboard.html')
	else:
		form = SignupForm()
	return render(request, 'signup.html', {'form': form, 'response': ""})

def dashboard(request):
	return render(request, 'dashboard.html')

def forgot(request):
	if request.method == 'POST':
		form = ForgotForm(request.POST)
		if form.is_valid():
			e = form.cleaned_data['email']
			try:
				user = Users.objects.get(email=e)
				newpass = get_random_string(12)
				msg = Mail(
				    subject='Forget Password',
				    html_content='Use this temporary password to login.\n'+newpass,
				    from_email='StockIt@ppw.com',
				    to_emails=[e],
				)
				user.password = newpass
				user.save()
				sg = SendGridAPIClient('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')
				sg.send(msg)
				return render(request, 'forgot.html', {'form': form, 'response': "Email has been sent!"})
			except:
				return render(request, 'forgot.html', {'form': form, 'response': "Account not found!"})
	else:
		form = ForgotForm()
	return render(request, 'forgot.html', {'form': form, 'response': ""})
